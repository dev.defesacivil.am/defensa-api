type PresenterProps = {
  count: number
  result: Array<unknown>
  page: number
}

export class Presenter {
  static toHTTP({ result, count, page }: PresenterProps) {
    return {
      results: result,
      count,
      page,
    }
  }
}
