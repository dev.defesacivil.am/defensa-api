import { AppModule } from '@/infra/app.module'
import { INestApplication } from '@nestjs/common'
import { Test } from '@nestjs/testing'
import request from 'supertest'

describe('Find user authenticate (E2E)', () => {
  let app: INestApplication

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile()

    app = moduleRef.createNestApplication()

    // prisma = moduleRef.get(PrismaService)

    await app.init()
  })

  test('[GET] /waterman/me', async () => {
    const { body } = await request(app.getHttpServer())
      .post('/authenticate/waterman')
      .send({
        email: 'victor@gmail.com',
        password: '123456',
      })

    const response = await request(app.getHttpServer())
      .get('/waterman/me')
      .set('Authorization', `Bearer ${body.accessToken}`)

    expect(response.statusCode).toBe(200)
  })
})
