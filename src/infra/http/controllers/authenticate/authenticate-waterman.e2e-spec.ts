import { AppModule } from '@/infra/app.module'
import { INestApplication } from '@nestjs/common'
import { Test } from '@nestjs/testing'
import request from 'supertest'

describe('Authenticate user waterman (E2E)', () => {
  let app: INestApplication

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile()

    app = moduleRef.createNestApplication()

    // prisma = moduleRef.get(PrismaService)

    await app.init()
  })

  test('[POST] /authenticate/waterman/', async () => {
    const response = await request(app.getHttpServer())
      .post('/authenticate/waterman')
      .send({
        email: 'victor@gmail.com',
        password: '123456',
      })

    console.log(response.body)
    expect(response.statusCode).toBe(201)
  })
})
