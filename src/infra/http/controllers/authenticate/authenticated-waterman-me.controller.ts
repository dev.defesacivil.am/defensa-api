import { AuthenticatedWatermanMeUseCase } from '@/domain/application/use-cases/authenticate/authenticated-waterman-me'
import { PermissionError } from '@/domain/application/use-cases/errors/permission-error'
import { CurrentUser } from '@/infra/auth/current-user-decorator'
import { UserPayload } from '@/infra/auth/jwt.strategy'
import {
  BadRequestException,
  Controller,
  Get,
  UnauthorizedException,
} from '@nestjs/common'
import { UserPresenter } from '../../presenters/user-presenter'

@Controller('/waterman/me')
export class AuthenticatedWatermanMeController {
  constructor(
    private authenticatedWatermanMe: AuthenticatedWatermanMeUseCase,
  ) {}

  @Get()
  async handle(@CurrentUser() user: UserPayload) {
    const userId = user.sub

    const response = await this.authenticatedWatermanMe.execute({ userId })

    if (response.isLeft()) {
      const error = response.value

      switch (error.constructor) {
        case PermissionError:
          throw new UnauthorizedException(error.message)
        default:
          throw new BadRequestException(error.message)
      }
    }

    return UserPresenter.toHTTP(response.value.user)
  }
}
