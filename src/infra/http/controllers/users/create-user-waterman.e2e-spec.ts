import { AppModule } from '@/infra/app.module'
import { PrismaService } from '@/infra/database/prisma/prisma.service'
import { INestApplication } from '@nestjs/common'
import { Test } from '@nestjs/testing'
import request from 'supertest'

describe('Create Waterman (E2E)', () => {
  let app: INestApplication
  let prisma: PrismaService

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile()

    app = moduleRef.createNestApplication()

    prisma = moduleRef.get(PrismaService)

    await app.init()
  })

  test('[POST] /waterman', async () => {
    // await prisma.riverChannel.create({
    //   data: {
    //     id: 'b6a12b6b-1d54-4645-8551-34de3c68718c',
    //     name: 'RIO NE NEGRO',
    //   },
    // })

    // await prisma.city.create({
    //   data: {
    //     id: '521db38b-c1fe-4738-9b90-3a621599f9a3',
    //     name: 'Manaus',
    //     riverChannelId: 'b6a12b6b-1d54-4645-8551-34de3c68718c',
    //     ibgeCode: '123445',
    //   },
    // })

    // await prisma.role.create({
    //   data: {
    //     id: 'fe46efae-9077-4ae9-8df5-5624c1057b66',
    //     name: 'DTI',
    //     description: 'Permisão para DTI',
    //   },
    // })

    const response = await request(app.getHttpServer()).post('/waterman').send({
      email: 'victor@gmail.com',
      password: '123456',
      firstName: 'VICTOR',
      lastName: 'CARVALHO',
      ssn: '050.202.232-98',
      rg: '123455',
      roleId: 'fe46efae-9077-4ae9-8df5-5624c1057b66',
      cityId: '521db38b-c1fe-4738-9b90-3a621599f9a3',
      phone: '93991386062',
      birthDate: '13-02-2002',
    })

    console.log(response.body)
    expect(response.statusCode).toBe(201)

    const userOnDatabase = await prisma.user.findUnique({
      where: {
        email: 'victor@gmail.com',
      },
    })

    console.log(userOnDatabase)
    expect(userOnDatabase).toBeTruthy()
  })
})
