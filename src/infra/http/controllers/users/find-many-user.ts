import { FindManyUserUseCase } from '@/domain/application/use-cases/users/find-many-user'
import { BadRequestException, Controller, Get, Query } from '@nestjs/common'
import { z } from 'zod'
import { ZodValidationPipe } from '../../pipes/zod-validation.pipe'
import { UserPresenter } from '../../presenters/user-presenter'
import { Public } from '@/infra/auth/public'

const pageQueryParamSchema = z
  .string()
  .optional()
  .default('1')
  .transform(Number)
  .pipe(z.number().min(1))

const queryValidationPipe = new ZodValidationPipe(pageQueryParamSchema)
type PageQueryParamSchema = z.infer<typeof pageQueryParamSchema>

@Controller('/users/')
@Public()
export class FindManyUserController {
  constructor(private findManyUser: FindManyUserUseCase) {}

  @Get()
  async handle(@Query('page', queryValidationPipe) page: PageQueryParamSchema) {
    const response = await this.findManyUser.execute({ page })

    if (response.isLeft()) {
      throw new BadRequestException()
    }

    const users = response.value.users

    return { users: users.map(UserPresenter.toHTTP) }
  }
}
