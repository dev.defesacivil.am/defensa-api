import { CreateUserWatermanUseCase } from '@/domain/application/use-cases/users/create-user-waterman'
import {
  BadRequestException,
  Body,
  Controller,
  HttpCode,
  Post,
  UsePipes,
} from '@nestjs/common'
import { z } from 'zod'
import { ZodValidationPipe } from '../../pipes/zod-validation.pipe'
import { Public } from '@/infra/auth/public'

const createWatermanBodySchema = z.object({
  email: z.string().email('Precisa se um email válido'),
  userName: z.string(),
  password: z.string(),
  firstName: z.string(),
  lastName: z.string(),
  ssn: z.string(),
  rg: z.string().optional(),
  roleId: z.string().uuid(),
  cityId: z.string().uuid(),
  phone: z.string(),
  birthDate: z.string(),
})

type CreateWatermanBodySchema = z.infer<typeof createWatermanBodySchema>

@Controller('/waterman/')
@Public()
export class CreateUserWatermanController {
  constructor(private createUserWaterman: CreateUserWatermanUseCase) {}

  @Post()
  @HttpCode(201)
  @UsePipes(new ZodValidationPipe(createWatermanBodySchema))
  async handle(@Body() body: CreateWatermanBodySchema) {
    const response = await this.createUserWaterman.execute(body)

    if (response.isLeft()) {
      throw new BadRequestException(response.value.message)
    }
  }
}
