import { AppModule } from '@/infra/app.module'
import { INestApplication } from '@nestjs/common'
import { Test } from '@nestjs/testing'
import request from 'supertest'

describe('Find many users (E2E)', () => {
  let app: INestApplication

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [AppModule],
    }).compile()

    app = moduleRef.createNestApplication()

    // prisma = moduleRef.get(PrismaService)

    await app.init()
  })

  test('[GET] /users/', async () => {
    const response = await request(app.getHttpServer()).get('/users').send()

    console.log(response.body)
    expect(response.statusCode).toBe(200)
  })
})
