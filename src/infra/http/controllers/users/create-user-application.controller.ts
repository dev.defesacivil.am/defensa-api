import { CreateUserPermissionApplicationUseCase } from '@/domain/application/use-cases/users/create-user-permission-application'
import {
  BadRequestException,
  Body,
  Controller,
  HttpCode,
  Post,
  UsePipes,
} from '@nestjs/common'
import { z } from 'zod'
import { ZodValidationPipe } from '../../pipes/zod-validation.pipe'

const createBodySchema = z.object({
  userId: z.string().uuid(),
  applicationId: z.string().uuid(),
})

type CreateBodySchema = z.infer<typeof createBodySchema>

@Controller('/application')
export class CreateUserApplicationController {
  constructor(
    private createUserApplication: CreateUserPermissionApplicationUseCase,
  ) {}

  @Post()
  @HttpCode(201)
  @UsePipes(new ZodValidationPipe(createBodySchema))
  async handle(@Body() body: CreateBodySchema) {
    const response = await this.createUserApplication.execute(body)

    if (response.isLeft()) {
      throw new BadRequestException(response.value.message)
    }
  }
}
