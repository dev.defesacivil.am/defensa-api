import { Module } from '@nestjs/common'
import { CryptographyModule } from '../cryptography/cryptography.module'
import { CreateUserWatermanController } from './controllers/users/create-user-waterman.controller'
import { CreateUserWatermanUseCase } from '@/domain/application/use-cases/users/create-user-waterman'
import { FindManyUserController } from './controllers/users/find-many-user'
import { FindManyUserUseCase } from '@/domain/application/use-cases/users/find-many-user'
import { AuthenticateWatermanController } from './controllers/authenticate/authenticate-waterman.controller'
import { AuthenticaWatermanUseCase } from '@/domain/application/use-cases/authenticate/authenticate-waterman'
import { AuthenticatedWatermanMeController } from './controllers/authenticate/authenticated-waterman-me.controller'
import { AuthenticatedWatermanMeUseCase } from '@/domain/application/use-cases/authenticate/authenticated-waterman-me'

@Module({
  imports: [CryptographyModule],
  controllers: [
    CreateUserWatermanController,
    FindManyUserController,
    AuthenticateWatermanController,
    AuthenticatedWatermanMeController,
  ],
  providers: [
    CreateUserWatermanUseCase,
    FindManyUserUseCase,
    AuthenticaWatermanUseCase,
    AuthenticatedWatermanMeUseCase,
  ],
})
export class HttpModule {}
