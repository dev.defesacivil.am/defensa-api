import { Application } from '@/domain/enterprise/entities/application'

export class ApplicationPresenter {
  static toHTTP(raw: Application) {
    return {
      id: raw.id.toValue(),
      name: raw.name,
      description: raw.description,
      createdAt: raw.createdAt,
    }
  }
}
