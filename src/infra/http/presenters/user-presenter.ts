import { User } from '@/domain/enterprise/entities/users/user'
import { ApplicationPresenter } from './application-presenter'

export class UserPresenter {
  static toHTTP(user: User) {
    return {
      id: user.id.toValue(),
      name: `${user.firstName.toValue()} ${user.lastName.toValue()}`,
      userName: user.userName,
      ssn: user.ssn.toValue(),
      email: user.email,
      birthDate: user.birthDate,
      role: {
        id: user.role?.id.toValue(),
        name: user.role?.name,
        description: user.role?.description,
      },
      applications: user.applications?.map(ApplicationPresenter.toHTTP),
      roleId: user.roleId.toValue(),
      phone: user.phone,
    }
  }
}
