import { Role } from '@/domain/enterprise/entities/permission/role'

export class RolePresenter {
  static toHTTP(raw: Role) {
    return {
      id: raw.id.toString(),
      name: raw.name.toString(),
      description: raw.description,
      createdAt: raw.createdAt,
      updatedtAt: raw.updatedAt,
    }
  }
}
