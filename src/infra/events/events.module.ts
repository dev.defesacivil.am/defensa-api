import { Module } from '@nestjs/common'
import { DatabasesModule } from '../database/database.module'

@Module({
  imports: [DatabasesModule],
  providers: [],
})
export class EventsModule {}
