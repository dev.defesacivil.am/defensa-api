import { ApplicationRepository } from '@/domain/application/repositories/application-repository'
import { Application } from '@/domain/enterprise/entities/application'
import { Injectable } from '@nestjs/common'
import { PrismaService } from '../prisma.service'
import { PrismaApplicationMapper } from '../mappers/prismaapplication-mapper'

@Injectable()
export class PrismaApplicationRepository implements ApplicationRepository {
  constructor(private prisma: PrismaService) {}

  async create(application: Application): Promise<void> {
    const data = PrismaApplicationMapper.toPrisma(application)
    await this.prisma.application.create({ data })
  }

  async findMany(): Promise<Application[]> {
    const response = await this.prisma.application.findMany({
      orderBy: {
        name: 'desc',
      },
    })

    return response.map(PrismaApplicationMapper.toDomain)
  }

  async findUnique(id: string): Promise<Application | null> {
    const response = await this.prisma.application.findUnique({ where: { id } })

    if (!response) {
      return null
    }

    return PrismaApplicationMapper.toDomain(response)
  }
}
