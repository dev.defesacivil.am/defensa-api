import { Injectable } from '@nestjs/common'
import { PrismaService } from '../../prisma.service'
import { PrismaServiceMapper } from '../../mappers/permission/prisma-service-mapper'
import { Service } from '@/domain/enterprise/entities/permission/service'
import { ServiceRepository } from '@/domain/application/repositories/permission/service-repository'

@Injectable()
export class PrismaServiceRepository implements ServiceRepository {
  constructor(private prisma: PrismaService) {}

  async create(service: Service): Promise<Service> {
    const data = PrismaServiceMapper.toPrisma(service)
    const response = await this.prisma.service.create({ data })
    return PrismaServiceMapper.toDomain(response)
  }

  async findById(id: string): Promise<Service | null> {
    const response = await this.prisma.service.findUnique({ where: { id } })

    if (!response) {
      return null
    }

    return PrismaServiceMapper.toDomain(response)
  }

  async findMany(): Promise<Service[]> {
    const response = await this.prisma.service.findMany()
    return response.map(PrismaServiceMapper.toDomain)
  }

  async findManyByMethod(
    method: 'GET' | 'POST' | 'PATCH' | 'PUT' | 'DELTE',
  ): Promise<Service[]> {
    const response = await this.prisma.service.findMany({
      where: { method },
      orderBy: { endpoint: 'desc' },
    })

    return response.map(PrismaServiceMapper.toDomain)
  }

  async findManyBySearch(search: string): Promise<Service[]> {
    const response = await this.prisma.service.findMany({
      where: {
        OR: [
          {
            title: {
              contains: search,
              mode: 'insensitive',
            },
          },
          {
            description: {
              contains: search,
              mode: 'insensitive',
            },
          },
        ],
      },
    })

    return response.map(PrismaServiceMapper.toDomain)
  }

  async delete(id: string): Promise<void> {
    await this.prisma.$transaction([
      this.prisma.permission.deleteMany({ where: { serviceId: id } }),
      this.prisma.service.delete({ where: { id } }),
    ])
  }

  async isServiceMethodAndEndpointExist(
    method: 'GET' | 'POST' | 'PATCH' | 'PUT' | 'DELTE',
    endpoint: string,
  ): Promise<boolean> {
    const response = await this.prisma.service.findFirst({
      where: {
        method,
        endpoint,
      },
    })

    if (response) {
      return true
    }

    return false
  }
}
