import { Injectable } from '@nestjs/common'
import { PrismaService } from '../../prisma.service'
import { PrismaPermissionMapper } from '../../mappers/permission/prisma-permission-mapper'
import { Permission } from '@/domain/enterprise/entities/permission/permission'
import { PermissionRepository } from '@/domain/application/repositories/permission/permission-repository'

@Injectable()
export class PrismaPermissionRepository implements PermissionRepository {
  constructor(private prisma: PrismaService) {}

  async createMany(permission: Permission[]): Promise<number> {
    const data = permission.map(PrismaPermissionMapper.toPrisma)
    const response = await this.prisma.permission.createMany({ data })
    return response.count
  }

  async create(permission: Permission): Promise<Permission> {
    const data = PrismaPermissionMapper.toPrisma(permission)
    const response = await this.prisma.permission.create({ data })
    return PrismaPermissionMapper.toDomain(response)
  }

  async findManyByRole(roleId: string): Promise<Permission[]> {
    const response = await this.prisma.permission.findMany({
      where: { roleId },
    })

    return response.map(PrismaPermissionMapper.toDomain)
  }

  async delete(id: string): Promise<void> {
    await this.prisma.permission.delete({ where: { id } })
  }

  async deleteMany(id: string[]): Promise<void> {
    if (id.length === 0) {
      return
    }

    await this.prisma.permission.deleteMany({
      where: {
        id: {
          in: id,
        },
      },
    })
  }

  async isFindPermissionExists(
    roleId: string,
    serviceId: string,
  ): Promise<boolean> {
    const response = await this.prisma.permission.findFirst({
      where: { roleId, serviceId },
    })

    if (!response) {
      return false
    }

    return true
  }

  async findById(id: string): Promise<Permission | null> {
    const response = await this.prisma.permission.findUnique({ where: { id } })

    if (!response) {
      return null
    }

    return PrismaPermissionMapper.toDomain(response)
  }
}
