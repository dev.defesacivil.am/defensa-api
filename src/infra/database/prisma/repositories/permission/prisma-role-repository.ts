import { Injectable } from '@nestjs/common'
import { PrismaService } from '../../prisma.service'
import { PrismaRoleMapper } from '../../mappers/permission/prisma-role-mapper'
import { Role } from '@/domain/enterprise/entities/permission/role'
import { RoleRepository } from '@/domain/application/repositories/permission/role-repository'

@Injectable()
export class PrismaRoleRepository implements RoleRepository {
  constructor(private prisma: PrismaService) {}

  async create(role: Role): Promise<Role> {
    const data = PrismaRoleMapper.toPrisma(role)
    const response = await this.prisma.role.create({ data })
    return PrismaRoleMapper.toDomain(response)
  }

  async findMany(): Promise<Role[]> {
    const response = await this.prisma.role.findMany({
      where: { deletedAt: null },
      orderBy: { name: 'desc' },
    })

    return response.map(PrismaRoleMapper.toDomain)
  }

  async findById(id: string): Promise<Role | null> {
    const response = await this.prisma.role.findUnique({ where: { id } })

    if (!response) {
      return null
    }

    return PrismaRoleMapper.toDomain(response)
  }

  async findBySearch(search: string): Promise<Role[]> {
    const response = await this.prisma.role.findMany({
      where: {
        deletedAt: null,
        OR: [
          {
            name: {
              contains: search,
              mode: 'insensitive',
            },
          },
          {
            description: {
              contains: search,
              mode: 'insensitive',
            },
          },
        ],
      },
    })

    return response.map(PrismaRoleMapper.toDomain)
  }

  async update(role: Role): Promise<void> {
    const data = PrismaRoleMapper.toPrisma(role)
    await this.prisma.role.update({ where: { id: data.id }, data })
  }

  async delete(id: string): Promise<void> {
    await this.prisma.role.update({
      where: { id },
      data: {
        deletedAt: new Date(),
      },
    })
  }

  async findByName(name: string): Promise<Role | null> {
    const response = await this.prisma.role.findFirst({
      where: {
        name: {
          contains: name,
          mode: 'insensitive',
        },
      },
    })

    if (!response) {
      return null
    }

    return PrismaRoleMapper.toDomain(response)
  }
}
