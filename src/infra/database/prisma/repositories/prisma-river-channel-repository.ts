import { RiverChannelRepository } from '@/domain/application/repositories/river-channel-repository'
import { RiverChannel } from '@/domain/enterprise/entities/river-channel'
import { Injectable } from '@nestjs/common'
import { PrismaService } from '../prisma.service'
import { PrismaRiverChannelMapper } from '../mappers/prisma-river-channel-mapper'

@Injectable()
export class PrismaRiverChannelRepository implements RiverChannelRepository {
  constructor(private prisma: PrismaService) {}

  async create(river: RiverChannel): Promise<void> {
    const data = PrismaRiverChannelMapper.toPrisma(river)
    await this.prisma.riverChannel.create({ data })
  }

  async findMany(): Promise<RiverChannel[]> {
    const response = await this.prisma.riverChannel.findMany()

    return response.map(PrismaRiverChannelMapper.toDomain)
  }
}
