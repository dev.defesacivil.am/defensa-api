import { CityRepository } from '@/domain/application/repositories/city-repository'
import { City } from '@/domain/enterprise/entities/city'
import { Injectable } from '@nestjs/common'
import { PrismaService } from '../prisma.service'
import { PrismaCityMapper } from '../mappers/prisma-city-mapper'

@Injectable()
export class PrismaCityRepository implements CityRepository {
  constructor(private prisma: PrismaService) {}

  async create(city: City): Promise<void> {
    const data = PrismaCityMapper.toPrisma(city)
    await this.prisma.city.create({ data })
  }

  async findMany(): Promise<City[]> {
    const response = await this.prisma.city.findMany()
    return response.map(PrismaCityMapper.toDomain)
  }

  async findUnique(id: string): Promise<City | null> {
    const response = await this.prisma.city.findUnique({ where: { id } })

    if (!response) {
      return null
    }

    return PrismaCityMapper.toDomain(response)
  }
}
