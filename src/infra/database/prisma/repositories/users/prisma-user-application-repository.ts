import { UserApplicationRepository } from '@/domain/application/repositories/users/user-application-repository'
import { PrismaUserApplicationMapper } from '../../mappers/users/prisma-user-application-mapper'
import { UserApplication } from '@/domain/enterprise/entities/users/user-application'
import { PrismaService } from '../../prisma.service'
import { Injectable } from '@nestjs/common'

@Injectable()
export class PrismaUserApplicationRepository
  implements UserApplicationRepository
{
  constructor(private prisma: PrismaService) {}

  async create(application: UserApplication): Promise<void> {
    const data = PrismaUserApplicationMapper.toPrisma(application)
    await this.prisma.userApplication.create({ data })
  }

  async findUnique(userId: string, applicationId: string): Promise<boolean> {
    const response = await this.prisma.userApplication.findUnique({
      where: {
        applicationId_userId: {
          userId,
          applicationId,
        },
      },
    })

    if (!response) return false

    return true
  }

  async delete(userId: string, applicationId: string): Promise<void> {
    await this.prisma.userApplication.delete({
      where: {
        applicationId_userId: {
          userId,
          applicationId,
        },
      },
    })
  }
}
