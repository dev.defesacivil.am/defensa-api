import { DocumentRepository } from '@/domain/application/repositories/users/document-repository'
import { Document } from '@/domain/enterprise/entities/users/user-document'
import { PrismaService } from '../../prisma.service'
import { PrismaDocumentMapper } from '../../mappers/users/prisma-document-mapper'

export class PrismaDocumentRepository implements DocumentRepository {
  constructor(private prisma: PrismaService) {}

  async create(document: Document): Promise<void> {
    const data = PrismaDocumentMapper.toPrisma(document)
    await this.prisma.document.create({ data })
  }

  async findUnique(id: string): Promise<Document | null> {
    const response = await this.prisma.document.findUnique({ where: { id } })

    if (!response) {
      return null
    }

    return PrismaDocumentMapper.toDomain(response)
  }

  async update(document: Document): Promise<void> {
    const data = PrismaDocumentMapper.toPrisma(document)
    await this.prisma.document.update({ where: { id: data.id }, data })
  }

  async delete(id: string): Promise<void> {
    await this.prisma.document.update({
      where: { id },
      data: {
        deletedAt: new Date(),
      },
    })
  }
}
