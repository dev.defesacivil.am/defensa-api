import { PaginationParams } from '@/core/repositories/pagination-params'

import { Injectable } from '@nestjs/common'
import { PrismaService } from '../../prisma.service'
import { PrismaUserMapper } from '../../mappers/users/prisma-user-mapper'
import { UserRepository } from '@/domain/application/repositories/users/user-repository'
import { User } from '@/domain/enterprise/entities/users/user'

@Injectable()
export class PrismaUserRepository implements UserRepository {
  constructor(private prisma: PrismaService) {}

  async create(user: User): Promise<User> {
    const data = PrismaUserMapper.toPrisma(user)
    const response = await this.prisma.user.create({
      data,
      include: {
        documents: true,
        role: true,
        applications: {
          include: {
            application: true,
          },
        },
      },
    })
    return PrismaUserMapper.toDomain({
      ...response,
      applications: response.applications.map((app) => app.application),
    })
  }

  async findMany({ page }: PaginationParams): Promise<User[]> {
    const response = await this.prisma.user.findMany({
      where: { deletedAt: null },
      include: {
        documents: true,
        role: true,
        applications: {
          include: {
            application: true,
          },
        },
      },
      orderBy: {
        firstName: 'desc',
      },
      skip: (page - 1) * 10,
    })

    return response.map((res) =>
      PrismaUserMapper.toDomain({
        ...res,
        applications: res.applications.map((app) => app.application),
      }),
    )
  }

  async findManyByRoleId(
    { page }: PaginationParams,
    roleId: string,
  ): Promise<User[]> {
    const response = await this.prisma.user.findMany({
      where: { roleId, deletedAt: null },
      orderBy: {
        firstName: 'desc',
      },
      include: {
        documents: true,
        role: true,
        applications: {
          include: {
            application: true,
          },
        },
      },
      skip: (page - 1) * 10,
    })

    return response.map((res) =>
      PrismaUserMapper.toDomain({
        ...res,
        applications: res.applications.map((app) => app.application),
      }),
    )
  }

  async findManyByCityId(
    { page }: PaginationParams,
    cityId: string,
  ): Promise<User[]> {
    const response = await this.prisma.user.findMany({
      where: { cityId, deletedAt: null },
      orderBy: {
        firstName: 'desc',
      },
      include: {
        documents: true,
        role: true,
        applications: {
          include: {
            application: true,
          },
        },
      },
      skip: (page - 1) * 10,
    })

    return response.map((res) =>
      PrismaUserMapper.toDomain({
        ...res,
        applications: res.applications.map((app) => app.application),
      }),
    )
  }

  async findManyByStatusAndType(
    { page }: PaginationParams,
    status: 'ACCEPT' | 'WAITING' | 'BLOCKING' | 'NOT_AUTHORIZED',
  ): Promise<User[]> {
    const response = await this.prisma.user.findMany({
      where: { status, deletedAt: null },
      orderBy: {
        firstName: 'desc',
      },
      include: {
        documents: true,
        role: true,
        applications: {
          include: {
            application: true,
          },
        },
      },
      skip: (page - 1) * 10,
    })

    return response.map((res) =>
      PrismaUserMapper.toDomain({
        ...res,
        applications: res.applications.map((app) => app.application),
      }),
    )
  }

  async findManyByType({ page }: PaginationParams): Promise<User[]> {
    const response = await this.prisma.user.findMany({
      where: { deletedAt: null },
      orderBy: {
        firstName: 'desc',
      },
      include: {
        documents: true,
        role: true,
        applications: {
          include: {
            application: true,
          },
        },
      },
      skip: (page - 1) * 10,
    })

    return response.map((res) =>
      PrismaUserMapper.toDomain({
        ...res,
        applications: res.applications.map((app) => app.application),
      }),
    )
  }

  async findSearchBySsn(ssn: string): Promise<User[]> {
    const response = await this.prisma.user.findMany({
      where: {
        deletedAt: null,
        OR: [
          {
            ssn: {
              contains: ssn,
              mode: 'insensitive',
            },
          },
        ],
      },
      include: {
        documents: true,
        role: true,
        applications: {
          include: {
            application: true,
          },
        },
      },
    })

    return response.map((res) =>
      PrismaUserMapper.toDomain({
        ...res,
        applications: res.applications.map((app) => app.application),
      }),
    )
  }

  async findUniqueByEmail(email: string): Promise<User | null> {
    const response = await this.prisma.user.findFirst({
      where: {
        email: {
          contains: email,
          mode: 'insensitive',
        },
        deletedAt: null,
      },
      include: {
        documents: true,
        role: true,
        applications: {
          include: {
            application: true,
          },
        },
      },
    })

    if (!response) {
      return null
    }

    return PrismaUserMapper.toDomain({
      ...response,
      applications: response.applications.map((app) => app.application),
    })
  }

  async findUniqueById(id: string): Promise<User | null> {
    const response = await this.prisma.user.findUnique({
      where: { id, deletedAt: null },
      include: {
        documents: true,
        role: true,
        applications: {
          include: {
            application: true,
          },
        },
      },
    })

    if (!response) {
      return null
    }

    return PrismaUserMapper.toDomain({
      ...response,
      applications: response.applications.map((app) => app.application),
    })
  }

  async update(user: User): Promise<void> {
    const data = PrismaUserMapper.toPrisma(user)
    await this.prisma.user.update({
      where: { id: data.id },
      data,
    })
  }

  async updatePassword(id: string, password: string): Promise<void> {
    await this.prisma.user.update({ where: { id }, data: { password } })
  }

  async delete(id: string): Promise<void> {
    await this.prisma.user.update({
      where: { id },
      data: { deletedAt: new Date() },
    })
  }
}
