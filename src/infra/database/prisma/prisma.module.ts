import { Module } from '@nestjs/common'
import { PrismaService } from './prisma.service'
import { UserRepository } from '@/domain/application/repositories/users/user-repository'
import { PrismaUserRepository } from './repositories/users/prisma-user-repository'
import { PrismaPermissionRepository } from './repositories/permission/prisma-permission-repository'
import { PrismaApplicationRepository } from './repositories/prisma-application-repository'
import { ApplicationRepository } from '@/domain/application/repositories/application-repository'
import { PermissionRepository } from '@/domain/application/repositories/permission/permission-repository'
import { UserApplicationRepository } from '@/domain/application/repositories/users/user-application-repository'
import { PrismaUserApplicationRepository } from './repositories/users/prisma-user-application-repository'

@Module({
  imports: [],
  providers: [
    PrismaService,
    {
      provide: UserRepository,
      useClass: PrismaUserRepository,
    },
    {
      provide: PermissionRepository,
      useClass: PrismaPermissionRepository,
    },
    {
      provide: ApplicationRepository,
      useClass: PrismaApplicationRepository,
    },
    {
      provide: UserApplicationRepository,
      useClass: PrismaUserApplicationRepository,
    },
  ],
  exports: [
    PrismaService,
    UserRepository,
    PermissionRepository,
    ApplicationRepository,
    UserApplicationRepository,
  ],
})
export class PrismaModule {}
