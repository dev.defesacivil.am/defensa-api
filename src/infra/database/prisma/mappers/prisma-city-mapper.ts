import { UniqueEntityID } from '@/core/entities/unique-entity-id'
import { City } from '@/domain/enterprise/entities/city'
import { Prisma, City as PrismaCity } from '@prisma/client'

export class PrismaCityMapper {
  static toDomain(raw: PrismaCity): City {
    return City.create(
      {
        name: raw.name,
        ibgeCode: raw.ibgeCode,
        riverChannelId: new UniqueEntityID(raw.riverChannelId),
      },
      new UniqueEntityID(raw.id),
    )
  }

  static toPrisma(city: City): Prisma.CityUncheckedCreateInput {
    return {
      id: city.id.toValue(),
      name: city.name,
      ibgeCode: city.ibgeCode,
      riverChannelId: city.riverChannelId.toValue(),
    }
  }
}
