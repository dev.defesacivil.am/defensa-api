import { UniqueEntityID } from '@/core/entities/unique-entity-id'
import { RiverChannel } from '@/domain/enterprise/entities/river-channel'
import { Prisma, RiverChannel as PrismaRiverChannel } from '@prisma/client'

export class PrismaRiverChannelMapper {
  static toDomain(raw: PrismaRiverChannel): RiverChannel {
    return RiverChannel.create(
      {
        name: raw.name,
        createdAt: raw.createdAt,
      },
      new UniqueEntityID(raw.id),
    )
  }

  static toPrisma(data: RiverChannel): Prisma.RiverChannelUncheckedCreateInput {
    return {
      id: data.id.toValue(),
      name: data.name,
      createdAt: data.createdAt,
    }
  }
}
