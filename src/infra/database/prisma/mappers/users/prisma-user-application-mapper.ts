import { UniqueEntityID } from '@/core/entities/unique-entity-id'
import { UserApplication } from '@/domain/enterprise/entities/users/user-application'
import {
  Prisma,
  UserApplication as PrismaUserApplication,
} from '@prisma/client'

export class PrismaUserApplicationMapper {
  static toDomain(raw: PrismaUserApplication): UserApplication {
    return UserApplication.create({
      userId: new UniqueEntityID(raw.userId),
      applicationId: new UniqueEntityID(raw.applicationId),
    })
  }

  static toPrisma(
    data: UserApplication,
  ): Prisma.UserApplicationUncheckedCreateInput {
    return {
      userId: data.userId.toValue(),
      applicationId: data.applicationId.toValue(),
    }
  }
}
