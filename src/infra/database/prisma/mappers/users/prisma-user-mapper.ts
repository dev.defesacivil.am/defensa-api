import { UniqueEntityID } from '@/core/entities/unique-entity-id'
import { User } from '@/domain/enterprise/entities/users/user'
import {
  Prisma,
  User as PrismaUser,
  Document as PrismaDocument,
  Role as PrismaRole,
  Application as PrismaApplication,
} from '@prisma/client'
import { PrismaDocumentMapper } from './prisma-document-mapper'
import { PrismaRoleMapper } from '../permission/prisma-role-mapper'
import { Replace } from '@/domain/enterprise/entities/value-objects/replace'
import { PrismaApplicationMapper } from '../prisma-application-mapper'

type PrismaUserDetails = PrismaUser & {
  documents?: PrismaDocument[]
  role?: PrismaRole
  applications?: PrismaApplication[]
}

export class PrismaUserMapper {
  static toDomain(raw: PrismaUserDetails): User {
    return User.create(
      {
        firstName: Replace.create(raw.firstName),
        userName: raw.userName,
        lastName: Replace.create(raw.lastName),
        email: raw.email,
        password: raw.password,
        birthDate: raw.birthDate,
        phone: raw.phone,
        roleId: new UniqueEntityID(raw.roleId),
        cityId: new UniqueEntityID(raw.cityId),
        ssn: Replace.create(raw.ssn),
        rg: raw.rg,
        documents: raw.documents?.map(PrismaDocumentMapper.toDomain),
        role: PrismaRoleMapper.toDomain(raw.role as PrismaRole),
        status: raw.status,
        applications: raw.applications?.map(PrismaApplicationMapper.toDomain),
        createdAt: raw.createdAt,
        updatedAt: raw.updatedAt,
        deletedAt: raw.deletedAt,
      },
      new UniqueEntityID(raw.id),
    )
  }

  static toPrisma(user: User): Prisma.UserUncheckedCreateInput {
    return {
      id: user.id.toValue(),
      userName: user.userName,
      firstName: user.firstName.toValue(),
      lastName: user.lastName.toValue(),
      birthDate: user.birthDate,
      ssn: user.ssn.toValue(),
      rg: user.rg,
      email: user.email,
      password: user.password,
      phone: user.phone,
      roleId: user.roleId.toValue(),
      cityId: user.cityId.toValue(),
      status: user.status,
      createdAt: user.createdAt,
      updatedAt: user.updatedAt,
      deletedAt: user.deletedAt,
    }
  }
}
