import { UniqueEntityID } from '@/core/entities/unique-entity-id'
import { Document } from '@/domain/enterprise/entities/users/user-document'
import { Prisma, Document as PrismaDocument } from '@prisma/client'

export class PrismaDocumentMapper {
  static toDomain(raw: PrismaDocument): Document {
    return Document.create(
      {
        front: raw.front,
        verse: raw.verse,
        userId: new UniqueEntityID(raw.userId),
        createdAt: raw.createdAt,
        updatedAt: raw.updatedAt,
        deletedAt: raw.deletedAt,
      },
      new UniqueEntityID(raw.id),
    )
  }

  static toPrisma(document: Document): Prisma.DocumentUncheckedCreateInput {
    return {
      id: document.id.toValue(),
      front: document.front,
      verse: document.verse,
      userId: document.userId.toValue(),
      createdAt: document.createdAt,
      updatedAt: document.updatedAt,
      deletedAt: document.deletedAt,
    }
  }
}
