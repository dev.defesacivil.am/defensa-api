import { UniqueEntityID } from '@/core/entities/unique-entity-id'
import { Role } from '@/domain/enterprise/entities/permission/role'
import { Prisma, Role as PrismaRole } from '@prisma/client'

export class PrismaRoleMapper {
  static toDomain(raw: PrismaRole): Role {
    return Role.create(
      {
        name: raw.name,
        description: raw.description,
        createdAt: raw.createdAt,
        updatedAt: raw.updatedAt,
        deletedAt: raw.deletedAt,
      },
      new UniqueEntityID(raw.id),
    )
  }

  static toPrisma(data: Role): Prisma.RoleUncheckedCreateInput {
    return {
      id: data.id.toValue(),
      name: data.name,
      description: data.description,
      createdAt: data.createdAt,
      updatedAt: data.updatedAt,
      deletedAt: data.deletedAt,
    }
  }
}
