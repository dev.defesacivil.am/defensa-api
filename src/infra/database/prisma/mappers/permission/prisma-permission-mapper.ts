import { UniqueEntityID } from '@/core/entities/unique-entity-id'
import { Permission } from '@/domain/enterprise/entities/permission/permission'
import { Prisma, Permission as PrismaPermission } from '@prisma/client'

export class PrismaPermissionMapper {
  static toDomain(raw: PrismaPermission): Permission {
    return Permission.create({
      roleId: new UniqueEntityID(raw.roleId),
      serviceId: new UniqueEntityID(raw.serviceId),
    })
  }

  static toPrisma(data: Permission): Prisma.PermissionUncheckedCreateInput {
    return {
      roleId: data.roleId.toValue(),
      serviceId: data.serviceId.toValue(),
    }
  }
}
