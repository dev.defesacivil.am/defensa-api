import { UniqueEntityID } from '@/core/entities/unique-entity-id'
import { Service } from '@/domain/enterprise/entities/permission/service'
import { Prisma, Service as PrismaService } from '@prisma/client'

export class PrismaServiceMapper {
  static toDomain(raw: PrismaService): Service {
    return Service.create(
      {
        title: raw.title,
        description: raw.description,
        endpoint: raw.endpoint,
        method: raw.method,
      },
      new UniqueEntityID(raw.id),
    )
  }

  static toPrisma(data: Service): Prisma.ServiceUncheckedCreateInput {
    return {
      id: data.id.toValue(),
      title: data.title,
      description: data.description,
      endpoint: data.endpoint,
      method: data.method,
    }
  }
}
