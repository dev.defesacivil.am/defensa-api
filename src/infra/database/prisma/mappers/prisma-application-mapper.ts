import { UniqueEntityID } from '@/core/entities/unique-entity-id'
import { Application } from '@/domain/enterprise/entities/application'
import { Prisma, Application as PrismaApplication } from '@prisma/client'

export class PrismaApplicationMapper {
  static toDomain(raw: PrismaApplication): Application {
    return Application.create(
      {
        name: raw.name,
        description: raw.description,
        createdAt: raw.createdAt,
      },
      new UniqueEntityID(raw.id),
    )
  }

  static toPrisma(data: Application): Prisma.ApplicationUncheckedCreateInput {
    return {
      id: data.id.toValue(),
      name: data.name,
      description: data.description,
    }
  }
}
