import { Entity } from '@/core/entities/entity'
import { UniqueEntityID } from '@/core/entities/unique-entity-id'

type RiverChannelProps = {
  name: string
  createdAt?: Date
}

export class RiverChannel extends Entity<RiverChannelProps> {
  get name() {
    return this.props.name
  }

  get createdAt() {
    return this.props.createdAt
  }

  static create(props: RiverChannelProps, id?: UniqueEntityID) {
    return new RiverChannel(props, id)
  }
}
