export class Replace {
  private value: string

  constructor(value: string) {
    this.value = value
  }

  toValue() {
    return this.value
  }

  static create(value: string) {
    return new Replace(value)
  }

  static createTextNotSymbols(value: string) {
    const replace = value.replace(/\D/g, '')
    return new Replace(replace)
  }

  static createFormatText(value: string) {
    const removeSymbols = value.replace(/[^\w\s]/gi, '')
    const replace = removeSymbols
      .toLowerCase()
      .replace(/\b\w/g, (char) => char.toUpperCase())
    return new Replace(replace)
  }

  static createFormatTextUpperCase(value: string) {
    const replace = value.toUpperCase()
    return new Replace(replace)
  }
}
