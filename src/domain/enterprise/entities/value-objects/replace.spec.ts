import { Replace } from './replace'

describe('Create formatting text anda remove symbols', () => {
  test('it should be able to remove special characters from texts', () => {
    const text = Replace.createTextNotSymbols('050.202.232-98')
    expect(text.toValue()).toBe('05020223298')
  })

  test('it should be able to format text in uppercase and special character versions', () => {
    const text = Replace.createFormatText('VICTOR carv@alho')
    console.log(text.toValue())
    expect(text.toValue()).toBe('Victor Carvalho')
  })
})
