import { Entity } from '@/core/entities/entity'
import { UniqueEntityID } from '@/core/entities/unique-entity-id'

type ServiceProps = {
  method: 'GET' | 'POST' | 'PATCH' | 'PUT' | 'DELTE'
  endpoint: string
  title: string
  description: string
}

export class Service extends Entity<ServiceProps> {
  get method() {
    return this.props.method
  }

  get endpoint() {
    return this.props.endpoint
  }

  get title() {
    return this.props.title
  }

  get description() {
    return this.props.description
  }

  static create(props: ServiceProps, id?: UniqueEntityID) {
    return new Service(props, id)
  }
}
