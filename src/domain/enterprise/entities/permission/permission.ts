import { Entity } from '@/core/entities/entity'
import { UniqueEntityID } from '@/core/entities/unique-entity-id'

type PermissionProps = {
  serviceId: UniqueEntityID
  roleId: UniqueEntityID
}

export class Permission extends Entity<PermissionProps> {
  get roleId() {
    return this.props.roleId
  }

  get serviceId() {
    return this.props.serviceId
  }

  static create(props: PermissionProps) {
    return new Permission(props)
  }
}
