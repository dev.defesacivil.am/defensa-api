import { Entity } from '@/core/entities/entity'
import { UniqueEntityID } from '@/core/entities/unique-entity-id'

type RoleProps = {
  name: string
  description: string
  createdAt?: Date
  updatedAt?: Date
  deletedAt?: Date | null
}

export class Role extends Entity<RoleProps> {
  get name() {
    return this.props.name
  }

  get description() {
    return this.props.description
  }

  get createdAt() {
    return this.props.createdAt
  }

  get updatedAt() {
    return this.props.updatedAt
  }

  get deletedAt() {
    return this.props.deletedAt
  }

  static create(props: RoleProps, id?: UniqueEntityID) {
    return new Role(props, id)
  }
}
