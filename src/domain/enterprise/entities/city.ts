import { Entity } from '@/core/entities/entity'
import { UniqueEntityID } from '@/core/entities/unique-entity-id'

type CityProps = {
  name: string
  ibgeCode: string
  riverChannelId: UniqueEntityID
}

export class City extends Entity<CityProps> {
  get name() {
    return this.props.name
  }

  get ibgeCode() {
    return this.props.ibgeCode
  }

  get riverChannelId() {
    return this.props.riverChannelId
  }

  static create(props: CityProps, id?: UniqueEntityID) {
    return new City(props, id)
  }
}
