import { Entity } from '@/core/entities/entity'
import { UniqueEntityID } from '@/core/entities/unique-entity-id'
import { Document } from './user-document'
import { Role } from '../permission/role'
import { City } from '../city'
import { Replace } from '../value-objects/replace'
import { Application } from '../application'

type UserProps = {
  userName: string
  status: 'ACCEPT' | 'WAITING' | 'BLOCKING' | 'NOT_AUTHORIZED'
  email: string
  password: string
  firstName: Replace
  lastName: Replace
  ssn: Replace
  rg?: string | null
  roleId: UniqueEntityID
  cityId: UniqueEntityID
  phone: string
  birthDate: string
  createdAt?: Date
  updatedAt?: Date
  deletedAt?: Date | null

  documents?: Document[]
  role?: Role
  city?: City
  applications?: Application[]
}

export class User extends Entity<UserProps> {
  get status() {
    return this.props.status
  }

  get userName() {
    return this.props.userName
  }

  get email() {
    return this.props.email
  }

  set email(email: string) {
    this.props.email = email.toLowerCase()
  }

  get firstName() {
    return this.props.firstName
  }

  set firstName(firstName: Replace) {
    this.props.firstName = Replace.createFormatText(firstName.toValue())
  }

  get lastName() {
    return this.props.lastName
  }

  set lastName(lastName: Replace) {
    this.props.lastName = Replace.createFormatText(lastName.toValue())
  }

  get password() {
    return this.props.password
  }

  get ssn() {
    return this.props.ssn
  }

  set ssn(ssn: Replace) {
    this.props.ssn = Replace.createTextNotSymbols(ssn.toValue())
  }

  get rg() {
    return this.props.rg
  }

  get roleId() {
    return this.props.roleId
  }

  get cityId() {
    return this.props.cityId
  }

  get phone() {
    return this.props.phone
  }

  get birthDate() {
    return this.props.birthDate
  }

  get createdAt() {
    return this.props.createdAt
  }

  get updatedAt() {
    return this.props.updatedAt
  }

  get deletedAt() {
    return this.props.deletedAt
  }

  get documents() {
    return this.props.documents
  }

  get role() {
    return this.props.role
  }

  get applications() {
    return this.props.applications
  }

  static create(props: UserProps, id?: UniqueEntityID) {
    return new User(props, id)
  }
}
