import { Entity } from '@/core/entities/entity'
import { UniqueEntityID } from '@/core/entities/unique-entity-id'

type UserApplicationProps = {
  applicationId: UniqueEntityID
  userId: UniqueEntityID
}

export class UserApplication extends Entity<UserApplicationProps> {
  get applicationId() {
    return this.props.applicationId
  }

  get userId() {
    return this.props.userId
  }

  static create(props: UserApplicationProps) {
    return new UserApplication(props)
  }
}
