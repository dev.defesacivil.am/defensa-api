import { Entity } from '@/core/entities/entity'
import { UniqueEntityID } from '@/core/entities/unique-entity-id'

type DocumentProps = {
  verse: string
  front: string
  userId: UniqueEntityID
  createdAt?: Date
  updatedAt?: Date
  deletedAt?: Date | null
}

export class Document extends Entity<DocumentProps> {
  get verse() {
    return this.props.verse
  }

  get front() {
    return this.props.front
  }

  get userId() {
    return this.props.userId
  }

  get createdAt() {
    return this.props.createdAt
  }

  get updatedAt() {
    return this.props.updatedAt
  }

  get deletedAt() {
    return this.props.deletedAt
  }

  static create(props: DocumentProps, id?: UniqueEntityID) {
    return new Document(props, id)
  }
}
