import { Entity } from '@/core/entities/entity'
import { UniqueEntityID } from '@/core/entities/unique-entity-id'

type ApplicationProps = {
  name: string
  description: string
  createdAt?: Date
}

export class Application extends Entity<ApplicationProps> {
  get name() {
    return this.props.name
  }

  get description() {
    return this.props.description
  }

  get createdAt() {
    return this.props.createdAt
  }

  static create(props: ApplicationProps, id?: UniqueEntityID) {
    return new Application(props, id)
  }
}
