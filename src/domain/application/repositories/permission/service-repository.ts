import { Service } from '@/domain/enterprise/entities/permission/service'

export abstract class ServiceRepository {
  abstract create(service: Service): Promise<Service>
  abstract findMany(): Promise<Service[]>
  abstract findById(id: string): Promise<Service | null>
  abstract findManyByMethod(
    method: 'GET' | 'POST' | 'PATCH' | 'PUT' | 'DELTE',
  ): Promise<Service[]>

  abstract findManyBySearch(search: string): Promise<Service[]>
  abstract delete(id: string): Promise<void>
  abstract isServiceMethodAndEndpointExist(
    method: 'GET' | 'POST' | 'PATCH' | 'PUT' | 'DELTE',
    endpoint: string,
  ): Promise<boolean>
}
