import { Role } from '@/domain/enterprise/entities/permission/role'

export abstract class RoleRepository {
  abstract create(role: Role): Promise<Role>
  abstract findMany(): Promise<Role[]>
  abstract findById(id: string): Promise<Role | null>
  abstract findBySearch(search: string): Promise<Role[]>
  abstract update(role: Role): Promise<void>
  abstract delete(id: string): Promise<void>
  abstract findByName(name: string): Promise<Role | null>
}
