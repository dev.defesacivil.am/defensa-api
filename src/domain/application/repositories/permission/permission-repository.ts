import { Permission } from '@/domain/enterprise/entities/permission/permission'

export abstract class PermissionRepository {
  abstract createMany(permission: Permission[]): Promise<number>
  abstract findById(id: string): Promise<Permission | null>
  abstract create(permission: Permission): Promise<Permission>
  abstract findManyByRole(roleId: string): Promise<Permission[]>
  abstract delete(id: string): Promise<void>
  abstract deleteMany(id: string[]): Promise<void>
  abstract isFindPermissionExists(
    roleId: string,
    serviceId: string,
  ): Promise<boolean>
}
