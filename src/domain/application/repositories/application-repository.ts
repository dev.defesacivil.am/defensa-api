import { Application } from '@/domain/enterprise/entities/application'

export abstract class ApplicationRepository {
  abstract create(application: Application): Promise<void>
  abstract findMany(): Promise<Application[]>
  abstract findUnique(id: string): Promise<Application | null>
}
