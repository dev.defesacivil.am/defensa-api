import { RiverChannel } from '@/domain/enterprise/entities/river-channel'

export abstract class RiverChannelRepository {
  abstract create(data: RiverChannel): Promise<void>
  abstract findMany(): Promise<RiverChannel[]>
}
