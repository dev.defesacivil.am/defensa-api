import { City } from '@/domain/enterprise/entities/city'

export abstract class CityRepository {
  abstract create(city: City): Promise<void>
  abstract findMany(): Promise<City[]>
  abstract findUnique(id: string): Promise<City | null>
}
