import { UserApplication } from '@/domain/enterprise/entities/users/user-application'

export abstract class UserApplicationRepository {
  abstract create(data: UserApplication): Promise<void>
  abstract findUnique(userId: string, applicationId: string): Promise<boolean>
  abstract delete(userId: string, applicationId: string): Promise<void>
}
