import { PaginationParams } from '@/core/repositories/pagination-params'
import { User } from '@/domain/enterprise/entities/users/user'

export abstract class UserRepository {
  abstract create(user: User): Promise<User>
  abstract findMany(params: PaginationParams): Promise<User[]>
  abstract findManyByRoleId(
    params: PaginationParams,
    roleId: string,
  ): Promise<User[]>

  abstract findManyByCityId(
    params: PaginationParams,
    cityId: string,
  ): Promise<User[]>

  abstract findManyByStatusAndType(
    params: PaginationParams,
    status: 'ACCEPT' | 'WAITING' | 'BLOCKING' | 'NOT_AUTHORIZED',
  ): Promise<User[]>

  abstract findManyByType(params: PaginationParams): Promise<User[]>

  abstract findSearchBySsn(ssn: string): Promise<User[]>
  abstract findUniqueByEmail(email: string): Promise<User | null>
  abstract findUniqueById(id: string): Promise<User | null>
  abstract update(user: User): Promise<void>
  abstract updatePassword(id: string, password: string): Promise<void>
  abstract delete(id: string): Promise<void>

  // abstract findCount(): Promise<number>
}
