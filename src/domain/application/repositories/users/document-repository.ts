import { Document } from '@/domain/enterprise/entities/users/user-document'

export abstract class DocumentRepository {
  abstract create(document: Document): Promise<void>
  abstract findUnique(id: string): Promise<Document | null>
  abstract update(document: Document): Promise<void>
  abstract delete(id: string): Promise<void>
}
