import { UseCaseError } from '@/core/errors/use-case-error'

export class PermissionError extends Error implements UseCaseError {
  constructor() {
    super(`User type not permission.`)
  }
}
