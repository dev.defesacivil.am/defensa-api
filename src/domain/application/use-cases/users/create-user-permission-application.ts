import { Injectable } from '@nestjs/common'
import { UserApplicationRepository } from '../../repositories/users/user-application-repository'
import { Either, left, right } from '@/core/either'
import { UserApplication } from '@/domain/enterprise/entities/users/user-application'
import { UniqueEntityID } from '@/core/entities/unique-entity-id'
import { PermissionError } from '../errors/permission-error'

type CreateUserPermissionApplicationUseCaseRequest = {
  userId: string
  applicationId: string
}

type CreateUserPermissionApplicationUseCaseResponse = Either<
  PermissionError,
  null
>

@Injectable()
export class CreateUserPermissionApplicationUseCase {
  constructor(private userApplication: UserApplicationRepository) {}

  async execute({
    userId,
    applicationId,
  }: CreateUserPermissionApplicationUseCaseRequest): Promise<CreateUserPermissionApplicationUseCaseResponse> {
    const isExisting = await this.userApplication.findUnique(
      userId,
      applicationId,
    )

    if (isExisting) {
      return left(new PermissionError())
    }

    const createApplication = UserApplication.create({
      applicationId: new UniqueEntityID(applicationId),
      userId: new UniqueEntityID(userId),
    })

    await this.userApplication.create(createApplication)

    return right(null)
  }
}
