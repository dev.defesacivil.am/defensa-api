import { ApplicationRepository } from '@/domain/application/repositories/application-repository'
import { Application } from '@/domain/enterprise/entities/application'
import { Injectable } from '@nestjs/common'

type CreateApplicationUseCaseRequest = {
  name: string
  description: string
}

@Injectable()
export class CreateApplicationUseCase {
  constructor(private applicationRepository: ApplicationRepository) {}

  async execute({ name, description }: CreateApplicationUseCaseRequest) {
    const create = Application.create({ name, description })
    await this.applicationRepository.create(create)
  }
}
