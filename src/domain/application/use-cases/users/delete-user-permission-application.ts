import { Injectable } from '@nestjs/common'
import { UserApplicationRepository } from '../../repositories/users/user-application-repository'

type DeleteUserPermissionApplicationUseCaseRequest = {
  userId: string
  applicationId: string
}

@Injectable()
export class DeleteUserPermissionApplicationUseCase {
  constructor(private userApplicationRepo: UserApplicationRepository) {}

  async execute({
    userId,
    applicationId,
  }: DeleteUserPermissionApplicationUseCaseRequest) {
    await this.userApplicationRepo.delete(userId, applicationId)
  }
}
