import { Either, left, right } from '@/core/either'
import { UserRepository } from '@/domain/application/repositories/users/user-repository'
import { Injectable } from '@nestjs/common'
import { UserAlreadyExistsError } from '../errors/user-already-exists-error'
import { User } from '@/domain/enterprise/entities/users/user'
import { HashGenerator } from '../../cryptography/hash-generator'
import { UniqueEntityID } from '@/core/entities/unique-entity-id'
import { Replace } from '@/domain/enterprise/entities/value-objects/replace'

type CreateUserWatermanUseCaseRequest = {
  userName: string
  email: string
  password: string
  firstName: string
  lastName: string
  ssn: string
  rg?: string | null
  roleId: string
  cityId: string
  phone: string
  birthDate: string
}

type CreateUserWatermanUseCaseResponse = Either<UserAlreadyExistsError, null>

@Injectable()
export class CreateUserWatermanUseCase {
  constructor(
    private userRepository: UserRepository,
    private hashGenerator: HashGenerator,
  ) {}

  async execute(
    request: CreateUserWatermanUseCaseRequest,
  ): Promise<CreateUserWatermanUseCaseResponse> {
    const userExists = await this.userRepository.findUniqueByEmail(
      request.email,
    )

    if (userExists) {
      return left(new UserAlreadyExistsError(userExists.email))
    }

    const hashedPassword = await this.hashGenerator.hash(request.password)

    const data = User.create({
      ...request,
      firstName: Replace.createFormatText(request.firstName),
      lastName: Replace.createFormatText(request.lastName),
      ssn: Replace.createTextNotSymbols(request.ssn),
      password: hashedPassword,
      cityId: new UniqueEntityID(request.cityId),
      roleId: new UniqueEntityID(request.roleId),
      status: 'WAITING',
      createdAt: new Date(),
    })

    await this.userRepository.create(data)

    return right(null)
  }
}
