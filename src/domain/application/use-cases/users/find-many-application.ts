import { Either, right } from '@/core/either'
import { ApplicationRepository } from '@/domain/application/repositories/application-repository'
import { Application } from '@/domain/enterprise/entities/application'

type FindManyApplicationUseCaseReponse = Either<
  null,
  {
    applications: Application[]
  }
>

export class FindManyApplicationUseCase {
  constructor(private applicationRepository: ApplicationRepository) {}

  async execute(): Promise<FindManyApplicationUseCaseReponse> {
    const response = await this.applicationRepository.findMany()

    return right({
      applications: response,
    })
  }
}
