import { Either, left, right } from '@/core/either'
import { UserRepository } from '@/domain/application/repositories/users/user-repository'
import { User } from '@/domain/enterprise/entities/users/user'
import { Injectable } from '@nestjs/common'

type FindManyUserUseCaseRequest = {
  page: number
}

type FindManyUserUseCaseReponse = Either<
  null,
  {
    users: User[]
  }
>

@Injectable()
export class FindManyUserUseCase {
  constructor(private userRepository: UserRepository) {}

  async execute({
    page,
  }: FindManyUserUseCaseRequest): Promise<FindManyUserUseCaseReponse> {
    const response = await this.userRepository.findMany({ page })

    if (!response) {
      return left(null)
    }

    return right({
      users: response,
    })
  }
}
