import { Injectable } from '@nestjs/common'
import { Either, left, right } from '@/core/either'
import { AlreadyExistsError } from '@/core/errors/already-exists-error'
import { PermissionRepository } from '../repositories/permission-repository'

type DeleteManyPermissionUseCaseRequest = {
  permissionIds: string[]
}

type DeleteManyPermissionUseCaseResponse = Either<AlreadyExistsError, null>

@Injectable()
export class DeleteManyPermissionUseCase {
  constructor(private permissionRepository: PermissionRepository) {}

  async execute({
    permissionIds,
  }: DeleteManyPermissionUseCaseRequest): Promise<DeleteManyPermissionUseCaseResponse> {
    const permissionsExists = permissionIds.map(async (id) => {
      const exist = await this.permissionRepository.findById(id)

      if (!exist) return false

      return true
    })

    if ((await Promise.all(permissionsExists)).includes(true)) {
      return left(new AlreadyExistsError('Permission'))
    }

    await this.permissionRepository.deleteMany(permissionIds)

    return right(null)
  }
}
