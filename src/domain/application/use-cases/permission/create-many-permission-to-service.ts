import { Injectable } from '@nestjs/common'

import { Either, left, right } from '@/core/either'

import { UniqueEntityID } from '@/core/entities/unique-entity-id'
import { AlreadyExistsError } from '@/core/errors/already-exists-error'
import { PermissionRepository } from '../repositories/permission-repository'
import { Permission } from '@/domain/enterprise/entities/permission/permission'

type CreateManyPermissionToServiceUseCaseRequest = {
  roleId: string
  serviceId: string
}

type CreateManyPermissionToServiceUseCaseResponse = Either<
  AlreadyExistsError,
  null
>

@Injectable()
export class CreateManyPermissionToServiceUseCase {
  constructor(private permissionService: PermissionRepository) {}

  async execute(
    permissions: CreateManyPermissionToServiceUseCaseRequest[],
  ): Promise<CreateManyPermissionToServiceUseCaseResponse> {
    const permissionsExists = permissions.map(async (permission) => {
      const result = await this.permissionService.isFindPermissionExists(
        permission.roleId,
        permission.serviceId,
      )

      return result
    })

    if ((await Promise.all(permissionsExists)).includes(true)) {
      return left(new AlreadyExistsError('Permission'))
    }

    const createPermissions = permissions.map((permission) => {
      return Permission.create({
        roleId: new UniqueEntityID(permission.roleId),
        serviceId: new UniqueEntityID(permission.serviceId),
      })
    })

    await this.permissionService.createMany(createPermissions)

    return right(null)
  }
}
