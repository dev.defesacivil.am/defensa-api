import { Injectable } from '@nestjs/common'
import { Either, left, right } from '@/core/either'
import { ServiceRepository } from '@/domain/application/permission/repositories/service-repository'
import { Service } from '@/domain/enterprise/entities/permission/service'

type FindManyServiceByMethodUseCaseRequest = {
  method: 'GET' | 'POST' | 'PATCH' | 'PUT' | 'DELTE'
}

type FindManyServiceByMethodUseCaseResponse = Either<
  null,
  {
    services: Service[]
  }
>

@Injectable()
export class FindManyServiceByMethodUseCase {
  constructor(private serviceRepository: ServiceRepository) {}

  async execute({
    method,
  }: FindManyServiceByMethodUseCaseRequest): Promise<FindManyServiceByMethodUseCaseResponse> {
    const result = await this.serviceRepository.findManyByMethod(method)

    if (!result) {
      return left(null)
    }

    return right({
      services: result,
    })
  }
}
