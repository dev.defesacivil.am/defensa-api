import { Either, left, right } from '@/core/either'
import { AlreadyExistsError } from '@/core/errors/already-exists-error'
import { RoleRepository } from '@/domain/application/permission/repositories/role-repository'
import { Role } from '@/domain/enterprise/entities/permission/role'
import { Injectable } from '@nestjs/common'

type CreateRoleUseCaseRequest = {
  name: string
  description: string
  createdAt: Date
  updatedAt: Date
}

type CreateRoleUseCaseResponse = Either<
  AlreadyExistsError,
  {
    role: Role
  }
>

@Injectable()
export class CreateRoleUseCase {
  constructor(private roleRepository: RoleRepository) {}

  async execute({
    createdAt,
    name,
    description,
    updatedAt,
  }: CreateRoleUseCaseRequest): Promise<CreateRoleUseCaseResponse> {
    const roleExists = await this.roleRepository.findByName(name)

    if (roleExists) {
      return left(new AlreadyExistsError(name))
    }

    const data = Role.create({
      name,
      description,
      createdAt,
      updatedAt,
    })

    const response = await this.roleRepository.create(data)

    return right({
      role: response,
    })
  }
}
