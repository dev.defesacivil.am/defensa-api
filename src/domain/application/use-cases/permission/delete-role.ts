import { Injectable } from '@nestjs/common'
import { RoleRepository } from '@/domain/application/permission/repositories/role-repository'
import { Either, left, right } from '@/core/either'
import { ResourceNotFoundError } from '@/core/errors/resource-not-found-error'

type DeleteRoleUseCaseRequest = {
  id: string
}

type DeleteRoleUseCaseResponse = Either<ResourceNotFoundError, null>

@Injectable()
export class DeleteRoleUseCase {
  constructor(private RoleRepository: RoleRepository) {}

  async execute({
    id,
  }: DeleteRoleUseCaseRequest): Promise<DeleteRoleUseCaseResponse> {
    const roleExists = await this.RoleRepository.findById(id)

    if (roleExists) {
      return left(new ResourceNotFoundError())
    }

    await this.RoleRepository.delete(id)

    return right(null)
  }
}
