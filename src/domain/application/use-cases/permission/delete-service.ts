import { Either, left, right } from '@/core/either'
import { ResourceNotFoundError } from '@/core/errors/resource-not-found-error'
import { ServiceRepository } from '@/domain/application/permission/repositories/service-repository'
import { Injectable } from '@nestjs/common'

type DeleteServiceUseCaseRequest = {
  id: string
}

type DeleteServiceUseCaseResponse = Either<ResourceNotFoundError, null>

@Injectable()
export class DeleteServiceUseCase {
  constructor(private serviceRepository: ServiceRepository) {}

  async execute({
    id,
  }: DeleteServiceUseCaseRequest): Promise<DeleteServiceUseCaseResponse> {
    const result = await this.serviceRepository.findById(id)

    if (!result) {
      return left(new ResourceNotFoundError())
    }

    await this.serviceRepository.delete(id)

    return right(null)
  }
}
