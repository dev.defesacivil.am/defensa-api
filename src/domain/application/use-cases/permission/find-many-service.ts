import { Injectable } from '@nestjs/common'
import { Either, left, right } from '@/core/either'
import { ServiceRepository } from '@/domain/application/permission/repositories/service-repository'
import { Service } from '@/domain/enterprise/entities/permission/service'

type FindManyServiceUseCaseResponse = Either<
  null,
  {
    services: Service[]
  }
>

@Injectable()
export class FindManyServiceUseCase {
  constructor(private serviceRepository: ServiceRepository) {}

  async execute(): Promise<FindManyServiceUseCaseResponse> {
    const result = await this.serviceRepository.findMany()

    if (!result) {
      return left(null)
    }

    return right({
      services: result,
    })
  }
}
