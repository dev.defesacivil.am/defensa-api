import { Either, right } from '@/core/either'
import { RoleRepository } from '@/domain/application/permission/repositories/role-repository'
import { Role } from '@/domain/enterprise/entities/permission/role'

import { Injectable } from '@nestjs/common'

type FindManyRoleUseCaseResponse = Either<null, { role: Role[] }>

@Injectable()
export class FindManyRoleUseCase {
  constructor(private RoleRepository: RoleRepository) {}

  async execute(): Promise<FindManyRoleUseCaseResponse> {
    const response = await this.RoleRepository.findMany()

    return right({
      role: response,
    })
  }
}
