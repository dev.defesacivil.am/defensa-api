import { Injectable } from '@nestjs/common'
import { RoleRepository } from '@/domain/application/permission/repositories/role-repository'
import { Either, left, right } from '@/core/either'
import { Role } from '@/domain/enterprise/entities/permission/role'

type FindManyRoleBySearchUseCaseRequest = {
  search: string
}

type FindManyRoleBySearchUseCaseResponse = Either<
  null,
  {
    role: Role[]
  }
>

@Injectable()
export class FindManyRoleBySearchUseCase {
  constructor(private roleRepository: RoleRepository) {}

  async execute({
    search,
  }: FindManyRoleBySearchUseCaseRequest): Promise<FindManyRoleBySearchUseCaseResponse> {
    const response = await this.roleRepository.findBySearch(search)

    if (!response) {
      return left(null)
    }

    return right({
      role: response,
    })
  }
}
