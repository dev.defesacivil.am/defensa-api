import { Injectable } from '@nestjs/common'

import { Either, left, right } from '@/core/either'
import { Service } from '@/domain/enterprise/entities/permission/service'
import { ServiceRepository } from '../repositories/service-repository'

type FindManyServiceBySearchUseCaseRequest = {
  search: string
}

type FindManyServiceBySearchUseCaseResponse = Either<
  null,
  {
    services: Service[]
  }
>

@Injectable()
export class FindManyServiceBySearchUseCase {
  constructor(private serviceRepository: ServiceRepository) {}

  async execute({
    search,
  }: FindManyServiceBySearchUseCaseRequest): Promise<FindManyServiceBySearchUseCaseResponse> {
    const result = await this.serviceRepository.findManyBySearch(search)

    if (!result) {
      return left(null)
    }

    return right({
      services: result,
    })
  }
}
