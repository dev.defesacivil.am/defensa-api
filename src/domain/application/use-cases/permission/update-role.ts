import { Injectable } from '@nestjs/common'
import { RoleRepository } from '@/domain/application/permission/repositories/role-repository'
import { Either, left, right } from '@/core/either'
import { ResourceNotFoundError } from '@/core/errors/resource-not-found-error'
import { UniqueEntityID } from '@/core/entities/unique-entity-id'
import { Role } from '@/domain/enterprise/entities/permission/role'

type UpdateRoleUseCaseRequest = {
  id: string
  name: string
  description: string
  updatedAt: Date
}

type UpdateRoleUseCaseResponse = Either<ResourceNotFoundError, null>

@Injectable()
export class UpdateRoleUseCase {
  constructor(private roleRepository: RoleRepository) {}

  async execute({
    id,
    name,
    description,
    updatedAt,
  }: UpdateRoleUseCaseRequest): Promise<UpdateRoleUseCaseResponse> {
    const roleExists = await this.roleRepository.findById(id)

    if (!roleExists) {
      return left(new ResourceNotFoundError())
    }

    const update = Role.create(
      {
        name,
        description,
        updatedAt,
      },
      new UniqueEntityID(id),
    )

    await this.roleRepository.update(update)

    return right(null)
  }
}
