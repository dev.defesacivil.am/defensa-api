import { Injectable } from '@nestjs/common'

import { Either, left, right } from '@/core/either'

import { UniqueEntityID } from '@/core/entities/unique-entity-id'
import { AlreadyExistsError } from '@/core/errors/already-exists-error'
import { PermissionRepository } from '../repositories/permission-repository'
import { Permission } from '@/domain/enterprise/entities/permission/permission'

type CreatePermissionToServiceUseCaseRequest = {
  roleId: string
  serviceId: string
}

type CreatePermissionToServiceUseCaseResponse = Either<AlreadyExistsError, null>

@Injectable()
export class CreatePermissionToServiceUseCase {
  constructor(private permissionService: PermissionRepository) {}

  async execute({
    roleId,
    serviceId,
  }: CreatePermissionToServiceUseCaseRequest): Promise<CreatePermissionToServiceUseCaseResponse> {
    const permissionExists =
      await this.permissionService.isFindPermissionExists(roleId, serviceId)

    if (permissionExists) {
      return left(new AlreadyExistsError('Permission'))
    }

    const permission = Permission.create({
      roleId: new UniqueEntityID(roleId),
      serviceId: new UniqueEntityID(serviceId),
    })

    await this.permissionService.create(permission)

    return right(null)
  }
}
