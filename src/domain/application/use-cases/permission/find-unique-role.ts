import { Either, left, right } from '@/core/either'
import { ResourceNotFoundError } from '@/core/errors/resource-not-found-error'
import { RoleRepository } from '@/domain/application/permission/repositories/role-repository'
import { Role } from '@/domain/enterprise/entities/permission/role'

type FindUniqueRoleUseCaseRequest = {
  id: string
}

type FindUniqueRoleUseCaseResponse = Either<
  ResourceNotFoundError,
  {
    role: Role
  }
>

export class FindUniqueRoleUseCase {
  constructor(private roleRepository: RoleRepository) {}

  async execute({
    id,
  }: FindUniqueRoleUseCaseRequest): Promise<FindUniqueRoleUseCaseResponse> {
    const response = await this.roleRepository.findById(id)

    if (!response) {
      return left(new ResourceNotFoundError())
    }

    return right({
      role: response,
    })
  }
}
