import { Either, left, right } from '@/core/either'
import { AlreadyExistsError } from '@/core/errors/already-exists-error'
import { ResourceNotFoundError } from '@/core/errors/resource-not-found-error'
import { PermissionRepository } from '@/domain/application/permission/repositories/permission-repository'
import { Injectable } from '@nestjs/common'

type DeletePermissionUseCaseRequest = {
  id: string
}

type DeletePermissionUseCaseReponse = Either<AlreadyExistsError, null>

@Injectable()
export class DeletePermissionUseCase {
  constructor(private permissionRepository: PermissionRepository) {}

  async execute({
    id,
  }: DeletePermissionUseCaseRequest): Promise<DeletePermissionUseCaseReponse> {
    const permissionExists = await this.permissionRepository.findById(id)

    if (!permissionExists) {
      return left(new ResourceNotFoundError())
    }

    await this.permissionRepository.delete(id)

    return right(null)
  }
}
