import { Injectable } from '@nestjs/common'
import { Either, left, right } from '@/core/either'
import { AlreadyExistsError } from '@/core/errors/already-exists-error'
import { Service } from '@/domain/enterprise/entities/permission/service'
import { ServiceRepository } from '../repositories/service-repository'

type CreateServiceUseCaseRequest = {
  method: 'GET' | 'POST' | 'PATCH' | 'PUT' | 'DELTE'
  endpoint: string
  title: string
  description: string
}

type CreateServiceUseCaseResponse = Either<
  AlreadyExistsError,
  {
    service: Service
  }
>

@Injectable()
export class CreateServiceUseCase {
  constructor(private serviceRepository: ServiceRepository) {}

  async execute({
    title,
    description,
    endpoint,
    method,
  }: CreateServiceUseCaseRequest): Promise<CreateServiceUseCaseResponse> {
    const isService =
      await this.serviceRepository.isServiceMethodAndEndpointExist(
        method,
        endpoint,
      )

    if (isService) {
      return left(new AlreadyExistsError('Service'))
    }

    const data = Service.create({
      title,
      description,
      endpoint,
      method,
    })

    const service = await this.serviceRepository.create(data)

    return right({
      service,
    })
  }
}
