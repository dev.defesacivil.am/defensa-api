import { Injectable } from '@nestjs/common'
import { ServiceRepository } from '@/domain/application/permission/repositories/service-repository'
import { Either, left, right } from '@/core/either'
import { Service } from '@/domain/enterprise/entities/permission/service'

type FindUniqueServiceUseCaseRequest = {
  id: string
}

type FindUniqueServiceUseCaseResponse = Either<
  null,
  {
    service: Service
  }
>

@Injectable()
export class FindUniqueServiceUseCase {
  constructor(private serviceRepository: ServiceRepository) {}

  async execute({
    id,
  }: FindUniqueServiceUseCaseRequest): Promise<FindUniqueServiceUseCaseResponse> {
    const response = await this.serviceRepository.findById(id)

    if (!response) {
      return left(null)
    }

    return right({
      service: response,
    })
  }
}
