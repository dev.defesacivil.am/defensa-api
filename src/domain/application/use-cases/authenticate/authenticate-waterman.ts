import { PermissionRepository } from '@/domain/application/permission/repositories/permission-repository'
import { UserRepository } from '@/domain/application/repositories/users/user-repository'
import { HashComparer } from '../../cryptography/hash-comparer'
import { Encrypter } from '../../cryptography/encrypter'
import { Either, left, right } from '@/core/either'
import { WrongCredentialsError } from '../errors/wrong-credentials-error'
import { PermissionError } from '../errors/permission-error'
import { Injectable } from '@nestjs/common'

interface AuthenticaWatermanUseCaseRequest {
  email: string
  password: string
}

type AuthenticaWatermanUseCaseResponse = Either<
  PermissionError | WrongCredentialsError,
  {
    accessToken: string
    permissions: string[]
  }
>

@Injectable()
export class AuthenticaWatermanUseCase {
  constructor(
    private userRepository: UserRepository,
    private permissionRepository: PermissionRepository,
    private hashComparer: HashComparer,
    private encrypter: Encrypter,
  ) {}

  async execute({
    email,
    password,
  }: AuthenticaWatermanUseCaseRequest): Promise<AuthenticaWatermanUseCaseResponse> {
    const waterman = await this.userRepository.findUniqueByEmail(email)

    if (!waterman) {
      return left(new WrongCredentialsError())
    }

    if (waterman.type !== 'APP_COTA_RIO') {
      return left(new PermissionError())
    }

    const isPasswordValid = await this.hashComparer.compare(
      password,
      waterman.password,
    )

    if (!isPasswordValid) {
      return left(new WrongCredentialsError())
    }

    const accessToken = await this.encrypter.encrypt({
      sub: waterman.id.toString(),
    })

    const permissions = await this.permissionRepository.findManyByRole(
      waterman.roleId.toValue(),
    )

    const permissionIds = permissions.map((permission) =>
      permission.id.toValue(),
    )

    return right({
      accessToken,
      permissions: permissionIds,
    })
  }
}
