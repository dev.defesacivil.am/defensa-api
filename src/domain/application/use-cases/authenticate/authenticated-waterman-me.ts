import { Either, left, right } from '@/core/either'
import { UserRepository } from '@/domain/application/repositories/users/user-repository'
import { User } from '@/domain/enterprise/entities/users/user'
import { Injectable } from '@nestjs/common'
import { PermissionError } from '../errors/permission-error'

type AuthenticatedWatermanMeUseCaseRequest = {
  userId: string
}

type AuthenticatedWatermanMeUseCaseResponse = Either<
  PermissionError,
  {
    user: User
  }
>

@Injectable()
export class AuthenticatedWatermanMeUseCase {
  constructor(private userRepository: UserRepository) {}

  async execute({
    userId,
  }: AuthenticatedWatermanMeUseCaseRequest): Promise<AuthenticatedWatermanMeUseCaseResponse> {
    const response = await this.userRepository.findUniqueById(userId)

    if (!response) {
      return left(new PermissionError())
    }

    return right({
      user: response,
    })
  }
}
