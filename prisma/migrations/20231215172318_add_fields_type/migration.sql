/*
  Warnings:

  - You are about to drop the column `type` on the `users` table. All the data in the column will be lost.
  - You are about to drop the `alerts_waterman` table. If the table is not empty, all the data it contains will be lost.
  - You are about to drop the `quotas_waterman` table. If the table is not empty, all the data it contains will be lost.
  - A unique constraint covering the columns `[service_id,role_id]` on the table `permissions` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[user_name]` on the table `users` will be added. If there are existing duplicate values, this will fail.
  - Added the required column `application_type` to the `users` table without a default value. This is not possible if the table is not empty.
  - Added the required column `user_name` to the `users` table without a default value. This is not possible if the table is not empty.

*/
-- DropForeignKey
ALTER TABLE "alerts_waterman" DROP CONSTRAINT "alerts_waterman_city_id_fkey";

-- DropForeignKey
ALTER TABLE "alerts_waterman" DROP CONSTRAINT "alerts_waterman_waterman_id_fkey";

-- DropForeignKey
ALTER TABLE "quotas_waterman" DROP CONSTRAINT "quotas_waterman_city_id_fkey";

-- DropForeignKey
ALTER TABLE "quotas_waterman" DROP CONSTRAINT "quotas_waterman_waterman_id_fkey";

-- AlterTable
ALTER TABLE "users" DROP COLUMN "type",
ADD COLUMN     "application_type" "Type" NOT NULL,
ADD COLUMN     "user_name" TEXT NOT NULL,
ALTER COLUMN "status" SET DEFAULT 'WAITING';

-- DropTable
DROP TABLE "alerts_waterman";

-- DropTable
DROP TABLE "quotas_waterman";

-- CreateTable
CREATE TABLE "quotas" (
    "id" TEXT NOT NULL,
    "value" TEXT NOT NULL,
    "was_created" TIMESTAMP(3) NOT NULL,
    "lat" DOUBLE PRECISION,
    "long" DOUBLE PRECISION,
    "description" TEXT,
    "photo" TEXT,
    "application_type" "Type" NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,
    "deleted_at" TIMESTAMP(3),
    "user_id" TEXT NOT NULL,
    "city_id" TEXT NOT NULL,

    CONSTRAINT "quotas_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "alerts" (
    "id" TEXT NOT NULL,
    "is_expired" BOOLEAN NOT NULL,
    "date_expired" TIMESTAMP(3) NOT NULL,
    "content" TEXT NOT NULL,
    "application_type" "Type" NOT NULL,
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "updated_at" TIMESTAMP(3) NOT NULL,
    "deleted_at" TIMESTAMP(3),
    "user_id" TEXT NOT NULL,
    "city_id" TEXT NOT NULL,

    CONSTRAINT "alerts_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "permissions_service_id_role_id_key" ON "permissions"("service_id", "role_id");

-- CreateIndex
CREATE UNIQUE INDEX "users_user_name_key" ON "users"("user_name");

-- AddForeignKey
ALTER TABLE "quotas" ADD CONSTRAINT "quotas_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "quotas" ADD CONSTRAINT "quotas_city_id_fkey" FOREIGN KEY ("city_id") REFERENCES "cities"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "alerts" ADD CONSTRAINT "alerts_user_id_fkey" FOREIGN KEY ("user_id") REFERENCES "users"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "alerts" ADD CONSTRAINT "alerts_city_id_fkey" FOREIGN KEY ("city_id") REFERENCES "cities"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
