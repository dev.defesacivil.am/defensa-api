import 'dotenv/config'
import { execSync } from 'node:child_process'

// import { PrismaClient } from '@prisma/client'
// import { randomUUID } from 'node:crypto'

// const prisma = new PrismaClient()

// const schemaId = randomUUID()

// function generateUniqueDatabaseURL(schemaId: string) {
//   if (!process.env.DATABASE_URL) {
//     throw new Error('Please provider a DATABASE_URL environment variable.')
//   }
//   const url = new URL(process.env.DATABASE_URL)

//   url.searchParams.set('schema', schemaId)

//   return url.toString()
// }

beforeAll(async () => {
  // const databaseURL = generateUniqueDatabaseURL(schemaId)

  process.env.DATABASE_URL =
    'postgresql://development:d&f&s@2023development@localhost:5433/defense_db_development?schema=public'

  execSync('npx prisma migrate deploy')
})

// afterAll(async () => {
//   await prisma.$executeRawUnsafe(`DROP SCHEMA IF EXISTS "${schemaId}" CASCADE`)
//   await prisma.$disconnect()
// })
